<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Company::create([
           "name" => "Intel",
           "address" => "Alajuela"
        ]);

        \App\Models\Company::create([
            "name" => "TacoBell",
            "address" => "San Jose"
        ]);
    }
}
